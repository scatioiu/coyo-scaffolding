(function () {
  'use strict';

  function WorkspaceList() {
    var api = this;

    api.newButton = $('.actions-vertical a[ui-sref="main.workspace.create"]');
    api.searchInput = element(by.model('$ctrl.searchTerm'));
    api.search = function (workspaceName) {
      element(by.model('$ctrl.searchTerm')).sendKeys(workspaceName);
      return element.all(by.css('.workspace-card'));
    };

    api.filter = {
      all: $('.panel-filterbox').element(by.css('li[text-key="MODULE.WORKSPACES.FILTER.ALL"] a'))
    };

    api.list = {
      workspaceList: element.all(by.repeater('workspace in $ctrl.currentPage.content'))
    };

    api.workspaces = element.all(by.repeater('workspace in $ctrl.currentPage.content'));

    api.filterCategory = function (name) {
      element(by.cssContainingText('.filter-entry-options', name)).click();
    };

    api.navigateTo = function (elem) {
      elem.$('a[ng-click="$ctrl.open($event)"]').click();
    };
  }

  module.exports = WorkspaceList;

})();
