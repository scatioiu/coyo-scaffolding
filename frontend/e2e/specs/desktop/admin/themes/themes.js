(function () {
  'use strict';

  var login = require('./../../../login.page.js');
  var themes = require('./themes.page');

  describe('theme administration', function () {

    beforeEach(function () {
      login.loginDefaultUser();
      themes.get();
    });

    it('should set main background color', function () {
      themes.colors.mainBackground.clear();
      themes.colors.mainBackground.sendKeys('#000000');
      themes.saveButton.click();
      expect($('.container-admin').getCssValue('background-color')).toBe('rgba(0, 0, 0, 1)');
    });

    afterEach(function () {
      themes.get();
      themes.colors.mainBackground.clear();
      themes.saveButton.click();
    });
  });
})();
