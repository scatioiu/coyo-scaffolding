(function () {
  'use strict';

  function PageDetails() {
    var api = this;

    api.title = element(by.css('.page .title'));
    api.description = element(by.css('.page .description'));

    var imageHeaderContainer = $('.image-header');
    api.imageHeader = {
      changeCover: imageHeaderContainer.$('.change-cover'),
      subscribe: imageHeaderContainer.$('.page-subscribe')
    };

    api.group = {
      groupPanel: element.all(by.repeater('appGroup in $ctrl.sender.appNavigation')),
      groupPanelInput: function (index) {
        return api.group.groupPanel.get(index).$('.panel-body div');
      },
      groupPanelElements: function (index) {
        var firstElem = api.group.groupPanel.get(index);
        return firstElem.$$('.filter-entry');
      }
    };

    api.options = {
      addApp: $('.content-sidebar a[ng-click="$ctrl.addApp($ctrl.page, $ctrl.apps)"]'),
      addGroup: $('a[ng-click="$ctrl.addGroup()"]'),
      settings: $('.content-sidebar a[ui-sref="main.page.show.settings"]'),
      imprint: $('.content-sidebar a[ui-sref="main.page.show.imprint"]')
    };

    api.addApp = {
      blog: $('h5[translate="APP.BLOG.NAME"]'),
      timeline: $('h5[translate="APP.TIMELINE.NAME"]'),
      fileLibrary: $('h5[translate="APP.FILE_LIBRARY.NAME"]'),
      content: $('h5[translate="APP.CONTENT.NAME"]'),
      wiki: $('h5[translate="APP.WIKI.NAME"]'),
      save: $('button[form-ctrl="createAppForm"]'),
      back: $('button[ng-click="vm.goBack()"]')
    };

    api.settings = {
      saveButton: $('.btn-primary[label="SAVE"]'),
      deleteButton: $('a[ng-click="$ctrl.delete()"]')
    };

    api.fileLibraryTitle = $('.file-library-app h3');
  }

  module.exports = PageDetails;

})();
