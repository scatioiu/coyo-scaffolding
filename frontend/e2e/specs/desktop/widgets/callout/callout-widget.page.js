(function () {
  'use strict';

  var extend = require('util')._extend;
  function CalloutWidget(widget) {
    var api = extend(this, widget);

    api.callout = $('[ng-class="$ctrl.widget.settings._callout.alertClass"]');
    api.textarea = api.callout.$('textarea');
    api.calloutHasClass = function (cls) {
      return api.callout.getAttribute('class').then(function (classes) {
        return classes.split(' ').indexOf(cls) !== -1;
      });
    };

    api.inlineOptions = {
      styleToggle: widget.container.$('.widget-options span.btn-dark'),
      styleOption: function (label) {
        return widget.container.element(by.cssContainingText('.widget-options a', label));
      }
    };
  }
  module.exports = CalloutWidget;
})();
