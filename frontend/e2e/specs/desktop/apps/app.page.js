(function () {
  'use strict';

  function App() {
    var api = this;

    api.app = {
      blog: $('h5[translate="APP.BLOG.NAME"]'),
      timeline: $('h5[translate="APP.TIMELINE.NAME"]'),
      fileLibrary: $('h5[translate="APP.FILE_LIBRARY.NAME"]'),
      content: $('h5[translate="APP.CONTENT.NAME"]'),
      wiki: $('h5[translate="APP.WIKI.NAME"]'),
      list: $('h5[translate="APP.LIST.NAME"]'),
      forum: $('h5[translate="APP.FORUM.NAME"]'),
      save: $('button[form-ctrl="createAppForm"]'),
      back: $('button[ng-click="vm.goBack()"]')
    };

    api.appNav = {
      groupPanel: element.all(by.repeater('appGroup in $ctrl.sender.appNavigation')),
      groupPanelInput: function (index) {
        return api.appNav.groupPanel.get(index).$('.panel-body input');
      },
      groupPanelElements: function (index) {
        var firstElem = api.appNav.groupPanel.get(index);
        return firstElem.$$('.filter-entry');
      },
      hover: function (elementContainer) {
        browser.actions().mouseMove(elementContainer).perform();
      },
      openAppSettings: function (elem) {
        elem.$('.btn-settings').click();
      }
    };
  }

  module.exports = App;

})();
