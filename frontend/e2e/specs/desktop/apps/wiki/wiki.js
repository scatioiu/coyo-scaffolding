(function () {
  'use strict';

  var components = require('./../../../components.page');
  var WikiHelper = require('./wiki-helper');
  var WikiPage = require('./wiki.page');
  var login = require('./../../../login.page');
  var testHelper = require('../../../../testhelper');

  describe('wiki app', function () {
    var workspaceName, wikiHelper, wikiPage;

    beforeAll(function () {
      wikiHelper = new WikiHelper();
      wikiPage = new WikiPage();
      var key = Math.floor(Math.random() * 1000000);
      login.loginDefaultUser();

      // create workspace and navigate to it
      workspaceName = 'wiki-app-test-workspace-' + key;
      testHelper.createWorkspace(workspaceName);
    });

    beforeEach(function () {
      browser.get('/workspaces/' + workspaceName);
    });

    afterAll(function () {
      testHelper.deleteWorkspace();
    });

    it('create wiki app', function () {
      wikiHelper.createEmptyWikiApp();
    });

    it('create wiki article', function () {
      wikiHelper.createWikiArticle('article 1', 'article 1');
      expect(wikiPage.article.view.title.isPresent()).toBeTruthy();
      expect(wikiPage.article.view.text.isPresent()).toBeTruthy();
    });

    it('delete wiki article', function () {
      var article = wikiPage.articleList.articles.first();
      expect(article.isPresent()).toBeTruthy();

      wikiHelper.hoverElement(article.element(wikiPage.articleList.contextMenu.hover));
      article.element(wikiPage.articleList.contextMenu.toggle).click();
      article.element(wikiPage.articleList.contextMenu.delete).click();
      components.modals.confirm.deleteButton.click();

      expect(article.isPresent()).toBeFalsy();
    });

    it('edit a wiki article', function () {
      var changedTitle = 'changed title';
      var changedText = 'changed text';
      wikiHelper.createWikiArticle('article 1', 'article 1');

      wikiPage.article.view.overView.click();

      var article = wikiPage.articleList.articles.first();
      expect(article.isPresent()).toBeTruthy();

      wikiHelper.hoverElement(article.element(wikiPage.articleList.contextMenu.hover));
      article.element(wikiPage.articleList.contextMenu.toggle).click();
      article.element(wikiPage.articleList.contextMenu.edit).click();

      wikiPage.article.create.editTitle.clear().sendKeys(changedTitle);
      wikiPage.article.create.editorInput.clear().sendKeys(changedText);
      wikiPage.article.create.saveEditButton.click();

      var title = wikiPage.article.view.title;
      var text = wikiPage.article.view.text;
      expect(title).toHaveText(changedTitle);
      expect(text).toHaveText(changedText);

      wikiPage.article.view.overView.click();

      wikiHelper.hoverElement(article.element(wikiPage.articleList.contextMenu.hover));
      article.element(wikiPage.articleList.contextMenu.toggle).click();
      article.element(wikiPage.articleList.contextMenu.delete).click();
      components.modals.confirm.deleteButton.click();
    });

  });
})
();

