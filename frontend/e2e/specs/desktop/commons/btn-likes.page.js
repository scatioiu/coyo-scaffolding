(function () {
  'use strict';

  function BtnLikes(btnLikesElement) {
    var api = this;
    api.element = btnLikesElement.$('.btn-likes-handle');
    api.text = btnLikesElement.$('.btn-likes-text');
    api.count = btnLikesElement.$('.btn-likes-count');
    api.tooltipCondensed = $('.btn-likes-tooltip-condensed');
  }

  BtnLikes.create = function (context) {
    return new BtnLikes(context);
  };

  module.exports = BtnLikes;

})();
