import {browser} from 'protractor';
import {promise as wdpromise} from 'selenium-webdriver';
import {Form} from './components/Elements';
import {applyMixins} from '../../TypescriptUtil';
import * as URI from 'urijs';

export class Page {

    private url: string;

    constructor(url: string) {
        this.url = url;
    }

    public open(): wdpromise.Promise<any> {
        return browser.get(this.url);
    }

    public isDisplayed(): wdpromise.Promise<boolean> {
        return this.isUrlOpen();
    }

    public isUrlOpen(): wdpromise.Promise<boolean> {
        return browser.getCurrentUrl().then((url) => {
            const currentPath = '/' + new URI(url).relativeTo(browser.baseUrl).path();
            return currentPath === this.url;
        });
    }
}

applyMixins(Page, [Form]);
