import {$, by, element} from 'protractor';
import {ElementFinder} from 'protractor/built/element';

export class Navigation {

    public navbar: ElementFinder = $('.main-navigation .coyo-navbar');
    public navLeft: ElementFinder = element(by.class('.nav .nav-left'));
    public navSearch: ElementFinder = element(by.class('.nav .nav-search'));
    public navRight: ElementFinder = element(by.class('.nav .nav-right'));

    public pages: ElementFinder = this.navbar.$('a[ui-sref="main.page"]');

    public goToPages(): void {
        this.pages.click();
    }
}
