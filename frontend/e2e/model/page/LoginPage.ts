import {$, by, element, protractor} from 'protractor';
import {promise as wdpromise} from 'selenium-webdriver';
import {Form, TextField} from './components/Elements';
import {CoyoPage} from './CoyoPage';
import {User} from '../data/User';
import {applyMixins} from '../../TypescriptUtil';
import {Action, ActionHolder, Actions} from './components/Action';

export class LoginPage extends CoyoPage implements Form<LoginPage> {

    public username: TextField = new TextField('username', element(by.model('$ctrl.user.username')));
    public password: TextField = new TextField('password', element(by.model('$ctrl.user.password')));

    // inherited from HasActions
    public actions: ActionHolder = new ActionHolder();
    public isActionSelectable: (action: Actions | string) => wdpromise.Promise<boolean>;
    public setActions: (actions?: Action[]) => LoginPage;
    public addAction: (actionGenerator: (component: LoginPage) => Action) => LoginPage;
    public selectAction: (action: Actions | string) => LoginPage;
    // inherited from Form
    public fillForm: (data: any | any[]) => LoginPage;
    public doSubmit: () => LoginPage;

    constructor() {
        super('/f/login');
        const form = $('#login-form');
        this.addAction(() => Action.submitButton(form));
    }

    public login(user: User): wdpromise.Promise<any> {
        this.fillForm(user).doSubmit();
        return protractor.promise.fulfilled();
    }

    /*
        ssoLogin(): Page {
            return null;
        }
    */
}

applyMixins(LoginPage, [Form]);
