import {$, by, element, ElementFinder} from 'protractor';
import {promise as wdpromise} from 'selenium-webdriver';
import {CoyoPage} from './CoyoPage';

export class ConfigPage extends CoyoPage {

    private elemConfigure: ElementFinder = $('.state-front-configure');
    private elemUrl: ElementFinder = element(by.model('$ctrl.url'));
    private btnSubmit: ElementFinder = $('span[translate="MODULE.LOGIN.CONFIGURE.SUBMIT"]');

    constructor() {
        super('/f/configure');
    }

    public isDisplayed(): wdpromise.Promise<boolean> {
        return this.elemConfigure.isPresent();
    }

    public setServerUrl(url: string): ConfigPage {
        this.elemUrl.sendKeys(url);
        return this;
    }

    public submit(): wdpromise.Promise<any> {
        return this.btnSubmit.click();
    }
}
